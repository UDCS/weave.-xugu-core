﻿using Data.Model;
using Data_logic;
using Mapster;
using Microsoft.IdentityModel.Tokens;
using SqlSugar;
using SqlSugar.DbConvert;
using SqlSugar.Xugu;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using XuguClient;

namespace XuguTest
{
    internal class Program
    {
        static  void Main(string[] args)
        {
            var o = new UserService();
            var firstData = new T_USER()
            {
                ID = 1,
                C_BIGINT = 1,
                C_BINARY = new byte[] { 1 },
                C_BLOB = new byte[] { 1 },
                C_BOOLEAN = true,
                C_CHAR = "A",
                C_CLOB = "A",
                C_DATE = DateTime.Now,
                C_DATETIME = DateTime.Now.AddDays(-1),
                C_DATETIME_WITH_TIME_ZONE = DateTimeOffset.Now,
                C_DECIMAL = 1.1M,
                C_DOUBLE = 1.1,
                C_FLOAT = 1.1F,
                C_GUID = Guid.NewGuid(),
                C_INT = 1,
                C_INTEGER = 1,
                C_INTERVAL_DAY = "A",
                C_INTERVAL_DAY_TO_HOUR = "A",
                C_INTERVAL_DAY_TO_MINUTE = "A",
                C_INTERVAL_DAY_TO_SECOND = "A",
                C_INTERVAL_HOUR = "A",
                C_INTERVAL_HOUR_TO_MINUTE = "A",
                C_INTERVAL_HOUR_TO_SECOND = "A",
                C_INTERVAL_MINUTE = "A",
                C_INTERVAL_MINUTE_TO_SECOND = "A",
                C_INTERVAL_MONTH = "A",
                C_INTERVAL_SECOND = "A",
                C_INTERVAL_YEAR = "2001",
                C_INTERVAL_YEAR_TO_MONTH = "2",
                C_NCHAR = "A",
                C_NUMERIC = 1.1M,
                C_NVARCHAR = "A",
                C_ROWID = "A",
                C_TIMESTAMP = DateTime.Now,
                C_TIME = DateTimeOffset.Now.TimeOfDay,
                C_TINYINT = 1,
                C_VARCHAR = "A",
                C_TIMESTAMP_AUTO_UPDATE = DateTime.Now,
                C_TIME_WITH_TIME_ZONE = DateTime.Now.TimeOfDay

            };
            Task.Run(async () =>
            {
                var type = 1;
            if (type == 0)
            {
                var fr = o.Add(firstData).Result;
                //o.CreateModel(@"F:\!GZH\Models\南海气象数据支撑系统", allTable: true);
                var task = o.GetList();
                var data = task.LastOrDefault();
                data.ID = data.ID + 1;
                data.C_DATETIME = DateTime.Now;
                //data.C_BINARY = File.ReadAllBytes(@"C:\Users\28679\Desktop\项目网址(新).docx");
                //data.C_DATE = DateTime.Now.AddDays(-10);
                var r = o.Add(data).Result;
                ////////////////////////var d1 = await o.Delete(o => o.C_BIGINT >=154 && o.C_BIGINT <=166);
                ////////////////////////var d = await o.Delete(o=>o.ID>1);

                var u = o.Single<T_USER, int>(t => t.C_DATETIME > DateTime.Today).Result;
                u.C_DATETIME = DateTime.Now.AddDays(2);
                var b = o.Update(u).Result;
                Console.WriteLine("Hello World!");
            }
            if (type == 1)
            {

                var list = new List<T_USER>();
                for (var i = 0; i < 10000 * 1; i++)
                {
                    var currentData = firstData.Adapt<T_USER>();
                    currentData.ID = firstData.ID + 10 + i;
                    list.Add(currentData);
                }
                var o1 = await o.BulkCopy(list);
            }
}).Wait();
        }
    }

    public class UserService : BaseDataLogic<T_USER>
    {
        public UserService()
        {

            db.CodeFirst.InitTables(typeof(T_USER));
        }
        public List<T_USER> GetList()
        {
            return db.Queryable<T_USER>().ToPageList(1, 1);
        }
        public async Task<int> BulkCopy(List<T_USER> list)
        {
            ////方法1：1W26s,2W70s，大批量不要用自己分割
            ////可能会报[E14024] 事物内变更操作次数超过最大许可值
            ////SET max_trans_modify TO 100000; -- 设置事务变更最大数为十万
            ////SET max_trans_modify TO 0;      -- 设置成0后，虚谷数据库不再对事务变更进行数量限制
            //return await db.Insertable<T_USER>(list).PageSize(1000).ExecuteCommandAsync();

            //方法2 10W275s
            var result = 0;
            await db.Utilities.PageEachAsync(list, 2000, async list1 =>
           {
               //这里必须是List<T>，如果是IEnumerable<T>会被作为一个对象使用json处理
               result += await db.CopyNew().Insertable(list1).ExecuteCommandAsync();

           });
            return result;
            ////Fastest不支持
            ////await globalSTDB.Fastest<T>().PageSize(pageSize).BulkCopyAsync(listModels);
        }
    }

}
